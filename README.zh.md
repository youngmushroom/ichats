# iChats
## [English](./README.md)
## iChats是一个linux/ubuntu/debian的微信客户端.
iChats基于 [NodeJs](http://nodejs.cn/)和[electron](https://electronjs.org).
因为Tencent官方并没有为linux提供微信客户端，但是提供了网页版的微信。所以我做了基于网页版微信的iChats微信客户端。


## 如何使用？
### 1, 可以直接从发布版本获得 [latestrelease](https://github.com/youngmushroom/ichats/releases) 
祝您玩的开心！
### 2, 也可以从 [gitlab](https://gitlab.com/youngmushroom/ichats.git) 克隆:
```shell
git clone https://gitlab.com/youngmushroom/ichats.git
```
克隆之后, 安装依赖库
```shell
npm install
```
在项目的根目录运行打包脚本:<br><br>
免安装版打包脚本(你可以从[这里](https://github.com/youngmushroom/ichats/releases/download/v1.1.1/ichats_1.1.1_amd64.deb)直接获得最新免安装版)
```shell
npm run pack
```
安装版打包脚本(你可以从[这里](https://github.com/youngmushroom/ichats/releases/download/v1.1.1/ichats_1.1.1_amd64.zip)直接获得最新安装版)
```shell
npm run dist
```

#### 提示：
如果你的操作系统不是ubuntu/debian, 请自己打包应用程序.<br>

## 运行后的运行效果
![登录页面](./temp/login.png)
![二维码登录页面](./temp/qr.png)
![聊天页面](./temp/chatroom.png)
## 已经发布的历史版本可以从[这里]()获得
## 程序涉及到的隐私
郑重声明，该程序不会收集任何个人隐私，但使用了如下的相关技术：<br>
1, 在加载的页面中预加载了 [``inject.js``](https://github.com/youngmushroom/ichats/blob/master/src/main/js/inject.js) ，判断用户是否登录，以此来调整窗口大小适应页面，提高用户体验。<br>
2, 在 [``index.js``](https://github.com/youngmushroom/ichats/blob/master/src/main/index.js) 中，在用户退出后，清除cookie。<br>
除此之外，再无和用户隐私相关的操作。如果您觉得这侵犯了您的隐私，请勿安装，感谢您的支持，谢谢！

## 兼容性列表
其他linux平台请自测，希望得到你的[bug](https://github.com/youngmushroom/ichats/issues)反馈<br>
 OS     |     version   
--------|--------------
 Ubuntu |  16.04 LTS     

## LICENSE
### [MIT LICENSE](https://github.com/youngmushroom/ichats/blob/master/LICENSE)
