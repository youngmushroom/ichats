const fs = require("fs");
const { ipcRenderer } = require('electron');
const path = require("path");
var Mushroom = Mushroom || {};
(function (Mushroom) {
    let loadCssFiles = function () {
        return fs.readFileSync(path.join(__dirname, "../css/theme.css"));
    }
    let fileToString = function () {
        return loadCssFiles().toString();
    }
    window.addEventListener("DOMContentLoaded", () => {
        let str = fileToString();
        let style = document.createElement("style");
        style.type = "text/css"
        style.innerHTML = str;
        document.body.appendChild(style);
    });
    (function () {
        var getCookie = function () {
            let webwxDataTicket = "webwx_data_ticket";
            let cookie = document.cookie;
            if (!cookie) {
                return "";
            }
            for (var ticketKey = webwxDataTicket + "=", a = cookie.split(";"), n = 0; n < a.length; n++) {
                for (var ticketValue = a[n]; " " == ticketValue.charAt(0);)
                    ticketValue = ticketValue.substring(1);
                if (ticketValue.indexOf(ticketKey) != -1)
                    return ticketValue.substring(ticketKey.length, ticketValue.length);
            }
            return "";
        }
        let notifyRefreshPage = function () {
            ipcRenderer.send('asynchronous-message', { type: "signin", signin: true });
        }
        let notifyLogout = function () {
            ipcRenderer.send('asynchronous-message', { type: "signout", signin: false });
        }

        var sendRefreshPage = function () {
            let ticketTimer = setInterval(function () {
                var ticket = getCookie();
                if (ticket && "" != ticket) {
                    ticketTimer && clearInterval(ticketTimer);
                    notifyRefreshPage();
                }
            });
        }
        var sendLogout = function () {
            setInterval(function () {
                var ticket = getCookie();
                if (!ticket || "" === ticket) {
                    notifyLogout();
                } else {
                    notifyRefreshPage();
                }
            });
        }
        sendLogout();
        ipcRenderer.on('beforeLogin', (event, user) => {
            if (user._signin) {
            } else {
                sendRefreshPage();
            }
        })
    })();
})(Mushroom);