# iChats
## [中文](./README.zh.md)
## iChats is a browser for tencent wechat.
iChats is a browser that is based on [electron](https://electronjs.org). 
iChats is for ubuntu/debian or other linux os
#### ps：
If your os is not ubuntu/debian, please pack the app by yourself.

## How to install 
### 1, You can get from [latestrelease](https://github.com/youngmushroom/ichats/releases) 
enjoy yourself!
### 2, You can clone from [github](https://github.com/youngmushroom/ichats.git) using the followings code:
```shell
git clone https://github.com/youngmushroom/ichats.git
```
#### After cloned, run pack script:<br><br>
Unpacked and Run app directly
```shell
npm run pack
```
Packed and Install ``.deb`` file
```shell packed
npm run dist
```
## Compatibility
### Ubuntu 16.04 LTS
## LICENSE
### [MIT LICENSE](https://github.com/youngmushroom/ichats/blob/master/LICENSE)
