"use strict";
const { BrowserWindow, app, session, ipcMain, Menu, Tray } = require("electron");
const path = require("path");
const config = require("./resources/config/config.json");
let i18n = (function (lang) {
    switch (lang) {
        case "zh_CN.UTF-8" || "zh_CN:en_US:en":
            return require("./resources/i18n/resource.cn.json");
        default:
            return require("./resources/i18n/resource.en.json");
    };
})(process.env.LANG || process.env.LANGUAGE || process.env.LC_ALL || process.env.LC_MESSAGES);

/**
 * @namespace {Object} Mushroom - an object
 */
var Mushroom;
(function (Mushroom) {

    class User {
        constructor () {
            this._signin = false;
        }
        signin() {
            this._signin = true;
        }
        signout() {
            this._signin = false;
        }
        isSigin() {
            return this._signin;
        }
    }
    const MAX = {
        width: 1000,
        height: 700,
    }
    const MIN = {
        width: 380,
        height: 540,
    }
    /**
     * @class an electron process
     * @prop {IpcMain} ipcMain - main process
     * @prop {BrowserWindow} browser - an web browser
     * @prop {Boolean} isOffline - a flag describes net work status
     */
    class Process {
        constructor () {
            this.user = new User();
            this.client = new Client();
            this.client.onReady(() => {
                this.onUserInfoChanged();
            });
            this.client.onClose(() => {
                return this.user.isSigin();
            });
            this.client.init();
        }
        /**
         * @public 
         */
        onAfterClose(callback) {
            this.client.onAfterClose(callback);
        }

        /**
         * @public restore window
         * @returns {void}
         */
        restore() {
            if (!this.client) {
                return;
            }
            if (this.client.isMaximized()) {
                this.client.restore();
            }
            this.client.focus();
        }
        /**
         * @private
         * @returns {void}
         */
        onUserInfoChanged() {
            if (ipcMain) {
                ipcMain.on('asynchronous-message', (event, arg) => {
                    if (this.client.isDestroyed()) {
                        return;
                    }
                    if ('signin' === arg.type && arg.signin && !this.user.isSigin()) {
                        this.user.signin();
                        this.client.resize(MAX.width, MAX.height);
                    } else if ("signout" === arg.type && !arg.signin && this.user.isSigin()) {
                        this.user.signout();
                        this.client.resize(MIN.width, MIN.height);
                    }
                });
            }
            if (!this.user.isSigin()) {
                setTimeout(() => {
                    this.client.browser.webContents.send('beforeLogin', this.user);
                });
            }
        }
    }
    /**
     * @class export class, an process
     */
    Mushroom.Process = Process;
    /**
     * @class a builder, build an browser for {@link Process}
     */
    class Client {
        constructor () {
            this.browser = new BrowserWindow({
                width: MIN.width,
                height: MIN.height,
                show: false,
                frame: false,
                resizable: false,
                title: config.title,
                titleBarStyle: "hidden",
                webPreferences: {
                    devTools: true,
                    preload: path.join(__dirname, "./js/inject.js"),
                },
            });
            this.browser.setMenu(null);
            this.browser.setMenuBarVisibility(false);
            this.appIcon = undefined;
        }
        isMinimized() {
            if (this.browser) {
                return this.browser.isMaximized();
            }
            return false;
        }
        restore() {
            if (this.browser) {
                this.browser.restore();
            }
        }
        focus() {
            if (this.browser) {
                this.browser.focus();
            }
        }
        /**
         * init all event here
         * @public
         * @method
         * @returns {void}
         */
        init() {
            this.attachEvents();
            this.browser.loadURL(config.website);
        }
        /**
         * @private
         * @method
         * @returns {void}
         */
        attachEvents() {
            this.onBrowserCrashed();
            this.onReadyToShow();
            this.onMinimized();
            this.onPageTitleUpdated();
        }
        /**
         * add app title changed event, title is a const
         * @private
         * @returns {void}
         */
        onPageTitleUpdated() {
            if (this.browser) {
                this.browser.on('page-title-updated', (event) => {
                    event.preventDefault();
                });
            }
        }
        /**
         * put in tray while app minimized 
         * @private
         * @returns {void}
         */
        onMinimized() {
            if (this.browser) {
                let createMenuList = () => {
                    let _self = this;
                    return Menu.buildFromTemplate([{
                        label: i18n.tray.menuList[0].label,
                        click: (menuItem, browser, event) => {
                            if (!browser) {
                                browser = _self.browser;
                            }
                            if (browser.isVisible()) {
                                browser.hide();
                            } else {
                                browser.show();
                                // hide app task bar
                                browser.setSkipTaskbar(false);
                                _self.appIcon.destroy();
                            }
                        }
                    }, {
                        label: i18n.tray.menuList[1].label,
                        click: (menuItem, browser, event) => {
                            if (!browser) {
                                browser = _self.browser;
                            }
                            if (_self.appIcon && !_self.appIcon.isDestroyed()) {
                                _self.appIcon.destroy();
                            }
                            _self.close();
                        }
                    }]);
                }
                this.browser.on("minimize", (event) => {
                    const iconPath = path.join(__dirname, './resources/icon/tray_icon.png');
                    if (!this.appIcon || this.appIcon.isDestroyed()) {
                        // create tray icon
                        let appIcon = new Tray(iconPath);
                        // click event does not work for linux 
                        // refer to: https://github.com/electron/electron/blob/master/docs/api/tray.md
                        // create menu for tray icon
                        let menu = createMenuList();
                        appIcon.setContextMenu(menu);
                        appIcon.setTitle(i18n.tray.title);
                        this.appIcon = appIcon;
                    }
                    this.browser.setSkipTaskbar(true);
                });

            }
        }
        /**
         * show app while loaded ready
         * @private
         * @method
         * @returns {void}
         */
        onReadyToShow() {
            if (this.browser) {
                this.browser.once('ready-to-show', () => {
                    this.browser.show();
                    if (this.callback) {
                        this.callback();
                    }
                });
            }
        }
        /**
         * @public
         * @method
         * @returns {void}
         */
        onReady(callback) {
            this.callback = callback;
        }
        /**
         * @private
         * @method
         * @returns {void}
         */
        onBrowserCrashed() {
            if (this.browser) {
                this.browser.webContents.on("crashed", () => {
                    this.browser.destroyAppIcon();
                    this.browser.destroy();
                });
            }
        }
        /**
         * destroy tray while tray is exist
         * @private
         * @returns {void}
         */
        destroyAppIcon() {
            if (this.appIcon && !this.appIcon.isDestroyed()) {
                this.appIcon.destroy();
            }
        }
        /**
         * @public
         * @param {Function} callback 
         * @returns {void}
         */
        onClose(callback) {
            if (this.browser) {
                this.browser.on("close", (e) => {
                    this.destroyAppIcon();
                    this.close();
                });
            }
        }
        /**
         * @public
         * @returns {boolean} 
         */
        isDestroyed() {
            return this.browser.isDestroyed();
        }
        /**
         * resize window size
         * @public
         * @param {number} width 
         * @param {number} height 
         * @returns {void}
         */
        resize(width, height) {
            if (this.isDestroyed()) {
                return;
            }
            let size = this.browser.getSize();
            if (size && 2 === size.length) {
                size[0] !== width && size[1] !== height && this.browser.setSize(width, height, true);
            }
        }
        /**
         * clear user cookies and destroy browser
         * @public
         * @method
         * @returns {void}
         */
        close() {
            session.defaultSession.cookies.remove(config.website, "webwx_data_ticket", () => { });
            this.browser.destroy();
            this.afterClose();
        }
        /**
         * @public attach event after browser close
         * @param {function} callback 
         */
        onAfterClose(callback) {
            this._afterClose = callback;
        }
        /**
         * @private call after close browser
         */
        afterClose() {
            if (this._afterClose) {
                this._afterClose.call(this);
            }
        }
    }
})(Mushroom || (Mushroom = {}));
((app, Mushroom) => {
    let instance = Mushroom.instance;
    const shouldQuit = app.makeSingleInstance((avg, workingDirectory) => {
        if (instance && instance.restore) {
            instance.restore();
        }
    });
    if (shouldQuit) {
        app.quit();
    }
    app.on('ready', () => {
        Mushroom.instance = new Mushroom.Process();
        Mushroom.instance.onAfterClose(() => {
            app.quit();
        });
    });
})(app, Mushroom);